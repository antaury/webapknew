﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class ZamestnanecsController : ApiController
    {
        private WebAppDataModel db = new WebAppDataModel();

        // GET: api/Zamestnanecs
        public IQueryable<Zamestnanec> GetZamestnanec()
        {
            return db.Zamestnanec;
        }

        // GET: api/Zamestnanecs/5
        [ResponseType(typeof(Zamestnanec))]
        public IHttpActionResult GetZamestnanec(int id)
        {
            Zamestnanec zamestnanec = db.Zamestnanec.Find(id);
            if (zamestnanec == null)
            {
                return NotFound();
            }

            return Ok(zamestnanec);
        }

        // PUT: api/Zamestnanecs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutZamestnanec(int id, Zamestnanec zamestnanec)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != zamestnanec.Id)
            {
                return BadRequest();
            }

            db.Entry(zamestnanec).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ZamestnanecExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Zamestnanecs
        [ResponseType(typeof(Zamestnanec))]
        public IHttpActionResult PostZamestnanec(Zamestnanec zamestnanec)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Zamestnanec.Add(zamestnanec);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = zamestnanec.Id }, zamestnanec);
        }

        // DELETE: api/Zamestnanecs/5
        [ResponseType(typeof(Zamestnanec))]
        public IHttpActionResult DeleteZamestnanec(int id)
        {
            Zamestnanec zamestnanec = db.Zamestnanec.Find(id);
            if (zamestnanec == null)
            {
                return NotFound();
            }

            db.Zamestnanec.Remove(zamestnanec);
            db.SaveChanges();

            return Ok(zamestnanec);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ZamestnanecExists(int id)
        {
            return db.Zamestnanec.Count(e => e.Id == id) > 0;
        }
    }
}