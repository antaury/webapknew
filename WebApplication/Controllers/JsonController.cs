﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class JsonController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Json
        public IQueryable<Zamestnanec> GetZamestnanecs()
        {
            return db.Zamestnanecs;
        }

        // GET: api/Json/5
        [ResponseType(typeof(Zamestnanec))]
        public IHttpActionResult GetZamestnanec(int id)
        {
            Zamestnanec zamestnanec = db.Zamestnanecs.Find(id);
            if (zamestnanec == null)
            {
                return NotFound();
            }

            return Ok(zamestnanec);
        }

        // PUT: api/Json/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutZamestnanec(int id, Zamestnanec zamestnanec)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != zamestnanec.Id)
            {
                return BadRequest();
            }

            db.Entry(zamestnanec).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ZamestnanecExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Json
        [ResponseType(typeof(Zamestnanec))]
        public IHttpActionResult PostZamestnanec(Zamestnanec zamestnanec)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Zamestnanecs.Add(zamestnanec);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = zamestnanec.Id }, zamestnanec);
        }

        // DELETE: api/Json/5
        [ResponseType(typeof(Zamestnanec))]
        public IHttpActionResult DeleteZamestnanec(int id)
        {
            Zamestnanec zamestnanec = db.Zamestnanecs.Find(id);
            if (zamestnanec == null)
            {
                return NotFound();
            }

            db.Zamestnanecs.Remove(zamestnanec);
            db.SaveChanges();

            return Ok(zamestnanec);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ZamestnanecExists(int id)
        {
            return db.Zamestnanecs.Count(e => e.Id == id) > 0;
        }
    }
}